<?php

/**
 * @file
 * Implementaion of the views 3 api hooks.
 */

/**
 * Implements hook_views_plugins().
 */
function views_mpdf_views_plugins() {

  $path = drupal_get_path('module', 'views_mpdf');
  $views_path = drupal_get_path('module', "views");

  return array(
    'display' => array(
      'v2pdf' => array(
        'title' => t('Export 2PDF'),
        'help' => t('Export view page to PDF file'),
        'path' => $path . '/plugins',
        'handler' => 'views_mpdf_plugin_display_page',
        'uses hook menu' => TRUE,
        'use ajax' => FALSE,
        'use pager' => FALSE,
        'accept attachments' => FALSE,
        'admin' => t('Export 2PDF'),
        'help topic' => 'display-export-2pdf',
        'type' => 'v2pdf',
        'theme' => 'views_view',
        'theme file' => $views_path . "/theme/theme.inc",
        'theme path' => $views_path . "/theme",
        'contextual links locations' => array()
      ),
    ),
    'style' => array(
      'v2pdf' => array(
        'title' => t('Export 2PDF Table'),
        'help' => t('PDF display table'),
        'path' => $path . '/plugins',
        'handler' => 'views_mpdf_plugin_style_table',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'v2pdf',
        'theme' => 'views_view_table',
        'theme file' => $views_path . "/theme/theme.inc",
        'theme path' => $views_path . "/theme"
      )
    )
  );
}
